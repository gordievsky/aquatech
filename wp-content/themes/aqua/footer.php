<?php
/**
 * The template for displaying the footer
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Aqua
 */
?>

</div><!-- .content -->

<footer class="footer">

	<div class='container'>
		<div class="footer-table table">
			<div class="cell"></div>
		</div>
	</div>

</footer><!-- .footer -->
</div><!-- .wrapper -->

<?php wp_footer(); ?>

</body>
</html>