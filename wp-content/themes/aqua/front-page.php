<?php
/**
 * The front page template file
 *
 * If the user has selected a static page for their homepage, this is what will
 * appear.
 * Learn more: https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Aqua
 */
get_header();
?>

<?php

while ( have_posts() ) : the_post(); ?>
<?php
$o = get_fields();
$themeUri = get_stylesheet_directory_uri();

$home = array(
	'bg' => isset($o['home_background']['url']) ? $o['home_background']['url'] : '',
	'title' => isset($o['home_title']) ? $o['home_title'] : '',
	'content' => isset($o['home_content']) ? $o['home_content'] : '',
);
$slider = array(
	1 => array(
		'title' => isset($o['slide1_title']) ? $o['slide1_title'] : '',
		'content' => isset($o['slide1_content']) ? $o['slide1_content'] : '',
		'image' => isset($o['slide1_image']['url']) ? $o['slide1_image']['url'] : '',
		'image_tablet' => isset($o['slide1_image_tablet']['url']) ? $o['slide1_image_tablet']['url'] : '',
		'image_mobile' => isset($o['slide1_image_mobile']['url']) ? $o['slide1_image_mobile']['url'] : '',
	),
	2 => array(
		'title' => isset($o['slide2_title']) ? $o['slide2_title'] : '',
		'content' => isset($o['slide2_content']) ? $o['slide2_content'] : '',
		'image' => isset($o['slide2_image']['url']) ? $o['slide2_image']['url'] : '',
	),
	3 => array(
		'title' => isset($o['slide3_title']) ? $o['slide3_title'] : '',
		'content' => isset($o['slide3_content']) ? $o['slide3_content'] : '',
		'image' => isset($o['slide3_image']['url']) ? $o['slide3_image']['url'] : '',
	),
	'fish_image' => isset($o['slider_fish_image']['url']) ? $o['slider_fish_image']['url'] : '',
);
$company = array(
	'main' => array(
		'title' => isset($o['our_company_title']) ? $o['our_company_title'] : '',
		'text' => isset($o['our_company_text']) ? $o['our_company_text'] : '',
		'logo' => isset($o['our_company_logo']['url']) ? $o['our_company_logo']['url'] : '',
		'text2' => isset($o['our_company_text_2']) ? $o['our_company_text_2'] : '',
		'text3' => isset($o['our_company_text_3']) ? $o['our_company_text_3'] : '',
		'image' => isset($o['our_company_image']['url']) ? $o['our_company_image']['url'] : '',
	),
	'tech' => array(
		'title' => isset($o['our_tech_title']) ? $o['our_tech_title'] : '',
		'text' => isset($o['our_tech__text']) ? $o['our_tech__text'] : '',
		'slider' => array(
			array(
				'url' => isset($o['our_tech_slide1']['url']) ? $o['our_tech_slide1']['url'] : ''
			),
			array(
				'url' => isset($o['our_tech_slide2']['url']) ? $o['our_tech_slide2']['url'] : ''
			),
			array(
				'url' => isset($o['our_tech_slide3']['url']) ? $o['our_tech_slide3']['url'] : ''
			),
			array(
				'url' => isset($o['our_tech_slide4']['url']) ? $o['our_tech_slide4']['url'] : ''
			),
			array(
				'url' => isset($o['our_tech_slide5']['url']) ? $o['our_tech_slide5']['url'] : ''
			),
			array(
				'url' => isset($o['our_tech_slide6']['url']) ? $o['our_tech_slide6']['url'] : ''
			),
			array(
				'url' => isset($o['our_tech_slide7']['url']) ? $o['our_tech_slide7']['url'] : ''
			),
			array(
				'url' => isset($o['our_tech_slide8']['url']) ? $o['our_tech_slide8']['url'] : ''
			),
		),
	),
	'facility' => array(
		'title' => isset($o['facility_operations_title']) ? $o['facility_operations_title'] : '',
		'text1' => isset($o['facility_operations_text1']) ? $o['facility_operations_text1'] : '',
		'text2' => isset($o['facility_operations_text2']) ? $o['facility_operations_text2'] : '',
		'text3' => isset($o['facility_operations_text3']) ? $o['facility_operations_text3'] : '',
	),
	'uvp' => array(
		'title' => isset($o['our_uvp_title']) ? $o['our_uvp_title'] : '',
		'text' => isset($o['our_uvp_text']) ? $o['our_uvp_text'] : '',
		'image' => isset($o['our_uvp_image']['url']) ? $o['our_uvp_image']['url'] : '',
		'items' => array(
			array(
				'title' => isset($o['item1_title']) ? $o['item1_title'] : '',
				'text' => isset($o['item1_text']) ? $o['item1_text'] : '',
			),
			array(
				'title' => isset($o['item2_title']) ? $o['item2_title'] : '',
				'text' => isset($o['item2_text']) ? $o['item2_text'] : '',
			),
			array(
				'title' => isset($o['item3_title']) ? $o['item3_title'] : '',
				'text' => isset($o['item3_text']) ? $o['item3_text'] : '',
			),
			array(
				'title' => isset($o['item4_title']) ? $o['item4_title'] : '',
				'text' => isset($o['item4_text']) ? $o['item4_text'] : '',
			),
			array(
				'title' => isset($o['item5_title']) ? $o['item5_title'] : '',
				'text' => isset($o['item5_text']) ? $o['item5_text'] : '',
			),
		)
	),
);
$species = array(
	'pre_title' => isset($o['our_species_pre_title']) ? $o['our_species_pre_title'] : '',
	'title' => isset($o['our_species_title']) ? $o['our_species_title'] : '',
	'text' => isset($o['our_species_text']) ? $o['our_species_text'] : '',
	'hidden_text' => isset($o['our_species_hidden_text']) ? $o['our_species_hidden_text'] : '',
	'image' => isset($o['our_species_image']['url']) ? $o['our_species_image']['url'] : '',
	'image_mob' => isset($o['our_species_image_mob']['url']) ? $o['our_species_image_mob']['url'] : '',
);
$team = array(
	'title' => isset($o['our_team_title']) ? $o['our_team_title'] : '',
	'text' => isset($o['our_team_text']) ? $o['our_team_text'] : '',
	'bottom_image' => isset($o['our_team_bottom_image']['url']) ? $o['our_team_bottom_image']['url'] : '',
);
$contact = array(
	'title' => isset($o['contact_us_title']) ? $o['contact_us_title'] : '',
//	'form' => isset($o['contact_us_form']) ? $o['contact_us_form'] : '',
	'phone' => isset($o['contact_us_phone']) ? $o['contact_us_phone'] : '',
	'email' => isset($o['contact_us_email']) ? $o['contact_us_email'] : '',
	'address' => isset($o['contact_us_address']) ? $o['contact_us_address'] : '',
);

?>

<div data-sector="1" class="block block-1 active" style="background-image: url(<?php echo $home['bg']; ?>);">
	<div class="block-1-table table">
		<div class="cell">
			<div class='container'>
				<div class="block-1__title"><?php echo $home['title']; ?></div>
				<div class="block-1__text"><?php echo $home['content']; ?></div>
			</div>
		</div>
	</div>
</div>
<div data-sector="2" class="block block-2">
	<div class="block-2-table table">
		<div class="cell">
			<div class="block-2-slider">
				<div class="block-2-slide block-2-slide-1">
					<div class='container'>
						<div class="block-2-slide-1__title"><?php echo $slider[1]['title']; ?></div>
						<div class="block-2-slide-1__text"><?php echo $slider[1]['content']; ?></div>
						<div class="block-2-slide-1-body">
							<div class="block-2-slide-1__image">
								<img class="i1" src="<?php echo $slider[1]['image']; ?>" alt="" />
								<img class="i2" src="<?php echo $slider[1]['image_tablet']; ?>" alt="" />
								<img class="i3" src="<?php echo $slider[1]['image_mobile']; ?>" alt="" />
							</div>
						</div>
					</div>
				</div>
				<div class="block-2-slide block-2-slide-2">
					<div class='container'>
						<div class="block-2-slide-2__image"><img src="<?php echo $slider[2]['image']; ?>" alt=""/></div>
						<div class="block-2-slide-2__text"><?php echo $slider[2]['content']; ?></div>
						<div class="block-2-slide-2__title"><?php echo $slider[2]['title']; ?></div>
					</div>
				</div>
				<div class="block-2-slide block-2-slide-3">
					<div class='container'>
						<div class="block-2-slide-3__image"><img src="<?php echo $slider[3]['image']; ?>" alt=""/></div>
						<div class="block-2-slide-3__title"><?php echo $slider[3]['title']; ?></div>
						<div class="block-2-slide-3__text"><?php echo $slider[3]['content']; ?></div>
					</div>
				</div>
			</div>
			<div class="block-2-slide__fish"><img src="<?php echo $slider['fish_image']; ?>" alt="" /></div>
		</div>
	</div>
</div>
<div data-sector="3" class="blocks block block-3">
	<div class="block-3-company">
		<div class="block-3-top">
			<div class='container'>
				<div class="block-3-top__title"><?php echo $company['main']['title']; ?></div>
				<div class="block-3-top__text"><?php echo $company['main']['text']; ?></div>
			</div>
		</div>
		<div class="block-3-body">
			<div class='container'>
				<div class="block-3-body__logo" style="background-image: url(<?php echo $company['main']['logo']; ?>)"></div>
				<div class="block-3-body__text">
					<?php echo $company['main']['text2']; ?>
					<div class="spoller ion-chevron-down"></div>
					<div class="hiddentext">
						<?php echo $company['main']['text3']; ?>
					</div>
				</div>
			</div>
		</div>
		<div class="block-3-image">
			<div class="block-3-image__value ibg"><img src="<?php echo $company['main']['image']; ?>" alt=""/></div>
		</div>
	</div>
	<div class="block-4">
		<div class="block-4__title"><?php echo $company['tech']['title']; ?></div>
		<div class="block-4__text"><?php echo $company['tech']['text']; ?></div>
		<div class="block-4-slider">
			<?php
			foreach ($company['tech']['slider'] as $slide) { ?>
				<div class="block-4-slide">
					<div class="block-4-slide__image ibg"><img src="<?php echo $slide['url']; ?>" alt=""/></div>
				</div>
			<?php } ?>
		</div>
	</div>
	<div class="block-5 anim">
		<div class='container'>
			<div class="block-5__title"><?php echo $company['facility']['title']; ?></div>
			<div class="block-5-body">
				<div class="block-5-table table">
					<div class="block-5-column">
						<div class="block-5-item block-5-item_1">
							<div class="block-5-item-body">
								<div class="block-5-item__num"><img src="<?php echo $themeUri . '/img/01.svg'; ?>" alt=""/></div>
								<div class="block-5-item__text"><?php echo $company['facility']['text1']; ?></div>
							</div>
						</div>
					</div>
					<div class="block-5-column">
						<div class="block-5-item block-5-item_2">
							<div class="block-5-item-body">
								<div class="block-5-item__num"><img src="<?php echo $themeUri . '/img/02.svg'; ?>" alt=""/></div>
								<div class="block-5-item__text"><?php echo $company['facility']['text2']; ?></div>
							</div>
						</div>
					</div>
					<div class="block-5-column">
						<div class="block-5-item block-5-item_3">
							<div class="block-5-item-body">
								<div class="block-5-item__num"><img src="<?php echo $themeUri . '/img/03.svg'; ?>" alt=""/></div>
								<div class="block-5-item__text"><?php echo $company['facility']['text3']; ?></div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="block-6">
		<div class='container'>
			<div class="block-6__title"><?php echo $company['uvp']['title']; ?></div>
			<div class="block-6__text"><?php echo $company['uvp']['text']; ?></div>
			<div class="block-6-fish">
				<div class="block-6-fish__image"><img src="<?php echo $company['uvp']['image']; ?>" alt=""/></div>
				<div class="block-6-fish-items table">
					<?php
					foreach ($company['uvp']['items'] as $index => $item) { ?>
						<div class="block-6-fish-item block-6-fish-item_<?php echo $index + 1; ?>">
							<div class="block-6-fish-item__title"><?php echo $item['title']; ?></div>
							<div class="block-6-fish-item__text"><?php echo $item['text']; ?></div>
						</div>
					<?php } ?>
				</div>
				<div class="block-6-fish-items mob table">
					<?php
					foreach (array(1, 4, 2, 5, 3) as $index) { ?>
						<div class="block-6-fish-item block-6-fish-item_<?php echo $index; ?>">
							<div class="block-6-fish-item__title"><?php echo $company['uvp']['items'][$index - 1]['title']; ?></div>
							<div class="block-6-fish-item__text"><?php echo $company['uvp']['items'][$index - 1]['text']; ?></div>
						</div>
					<?php } ?>
				</div>
			</div>
		</div>
	</div>
	<div class="block-7">
		<div class="block-7-table table">
			<div class="cell">
				<div class="block-7__pretitle"><?php echo $species['pre_title']; ?></div>
				<div class="block-7__title"><?php echo $species['title']; ?></div>
				<div class="block-7-image">
					<div class="block-7-image-table table">
						<div class="cell"><img src="<?php echo $species['image']; ?>" alt=""/></div>
					</div>
				</div>
				<div class="block-7-image__mob"><img src="<?php echo $species['image_mob']; ?>" alt=""/></div>
				<div class='container'>
					<div class="block-7-body">
						<div class="block-7__text">
							<?php echo $species['text']; ?>
							<div class="spoller spoller2 ion-chevron-down"></div>
							<div class="hiddentext2">
								<?php echo $species['hidden_text']; ?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="block-8">
		<div class='container'>
			<div class="block-8__title"><?php echo $team['title']; ?></div>
			<div class="block-8__text"><?php echo $team['text']; ?></div>
			<div class="block-8-body">
				<div class="block-8-table table">
					<?php
					$team_query = new WP_Query( array(
						'post_type' => 'aqua_team',
						'posts_per_page' => 10,
					) );
//					var_dump($team_query);
					if ( $team_query->have_posts() ) {
						while ( $team_query->have_posts() ) { $team_query->the_post(); ?>
							<div class="block-8-item-column">
								<div class="block-8-item">
									<div class="block-8-item__avatar"><?php the_post_thumbnail( 'full' ); ?></div>
									<div class="block-8-item__name"><?php the_title(); ?></div>
									<div class="block-8-item__position"><?php echo get_post_meta( get_the_ID(), 'aqua_team_position', true ); ?></div>
									<div class="spoller ion-chevron-down"></div>
									<div class="block-8-item__text hiddentext"><?php the_content(); ?></div>
								</div>
							</div>
						<?php }
					}
					wp_reset_postdata();
					?>
				</div>
			</div>
			<div class="block-8__image"><img src="<?php echo $team['bottom_image']; ?>" alt=""/></div>
		</div>
	</div>
	<div class="block-9">
		<div class='container'>
			<div class="block-9__title"><?php echo $contact['title']; ?></div>
				<?php echo do_shortcode('[contact-form-7 id="8" title="Contact form 1" html_class="form contact-form block-9-form"]'); ?>

			<div class="block-9-footer">
				<div class="block-9__phone"><?php echo $contact['phone']; ?></div>
				<a href="mailto:<?php echo $contact['email']; ?>" class="block-9__email"><?php echo $contact['email']; ?></a>
				<div class="block-9__footertext"><?php echo $contact['address']; ?></div>
			</div>
		</div>
	</div>
</div>

<?php
endwhile;
wp_reset_query();
?>

<?php
get_footer();