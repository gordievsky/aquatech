<?php

if ( ! function_exists( 'aqua_setup' ) )
{

	function aqua_setup()
	{
		load_theme_textdomain( 'aqua', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );
		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );
		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		register_nav_menus( [
			'header_main' => esc_html__( 'Primary', 'aqua' ),
		] );

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support( 'html5', [
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		] );

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support( 'custom-logo', [
			'height'      => 41,
			'width'       => 250,
			'flex-width'  => true,
			'flex-height' => true,
		] );
	}

	add_action( 'after_setup_theme', 'aqua_setup' );

}

function aqua_change_logo_atts( $html )
{
	$html = str_replace( 'custom-logo-link', 'header__logo', $html );
	$html = str_replace( 'custom-logo', '', $html );
	$html = preg_replace( '/(width|height)="\d*"/', '', $html );

	return $html;
}
add_filter( 'get_custom_logo', 'aqua_change_logo_atts' );

function aqua_body_class( $classes )
{
	$classes[] = 'scrlock';

	return $classes;
}
add_filter( 'body_class', 'aqua_body_class' );

/**
 * Get theme version
 */
function aqua_get_theme_version()
{
	$theme_info = wp_get_theme( get_template() );

	return $theme_info->get( 'Version' );
}

/**
 * Enqueue scripts and styles.
 */
function aqua_scripts()
{
	$ver = aqua_get_theme_version();
	$assets = get_stylesheet_directory_uri();

	// Styles
	if ( is_front_page() )
	{
		wp_enqueue_style( 'aqua', get_stylesheet_uri(), [], $ver );
		wp_enqueue_style( 'aqua-style', get_stylesheet_directory_uri() . '/css/style.css', [], $ver );
	}

	// Scripts
	if ( is_front_page() )
	{
		$jqDep = [ 'jquery' ];

		wp_enqueue_script( 'aqua-', "http://maps.google.com/maps/api/js?sensor=false&amp;key=AIzaSyDP0NQedbcnXkwDG0pjKzR3wXH_Rbon3n4", [], '', true );
//		wp_enqueue_script( 'aqua-', "http://code.jquery.com/jquery-latest.min.js", [], '', true );
//		wp_enqueue_script( 'aqua-', "http://code.jquery.com/ui/1.12.1/jquery-ui.min.js", [], '', true );

		wp_enqueue_script( 'jquery-inputmask-bundle', "{$assets}/js/jquery.inputmask.bundle.min.js", $jqDep, '', true );
		wp_enqueue_script( 'jquery-nicescroll', "{$assets}/js/jquery.nicescroll.js", $jqDep, '', true );
		wp_enqueue_script( 'jquery-mousewheel', "{$assets}/js/jquery.mousewheel.min.js", $jqDep, '', true );
		wp_enqueue_script( 'slick', "{$assets}/js/slick.min.js", $jqDep, '', true );
		wp_enqueue_script( 'velocity', "{$assets}/js/velocity.min.js", $jqDep, '', true );
		wp_enqueue_script( 'aqua-forms', "{$assets}/js/forms.js", $jqDep, '', true );
		wp_enqueue_script( 'aqua-script', "{$assets}/js/script.js", $jqDep, $ver, true );
	}
}

add_action( 'wp_enqueue_scripts', 'aqua_scripts' );

require_once 'inc/init.php';
