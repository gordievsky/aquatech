<?php
/**
 * The header for our theme
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Aqua
 */
?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<meta name="format-detection" content="telephone=no">
	<link rel="profile" href="http://gmpg.org/xfn/11">

	<link rel="shortcut icon" href="<?php echo get_stylesheet_directory_uri(); ?>/favicon.ico" />

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

<div class="wrapper">

	<header>
		<div class="container">
			<div class="header-table table">
				<div class="cell">
					<?php the_custom_logo(); ?>
				</div>
				<div class="cell full">
					<div class="header-menu__icon">
						<span></span>
						<span></span>
						<span></span>
					</div>
					<div class="header-menu">
						<?php
							wp_nav_menu( array(
								'theme_location' => 'header_main',
								'menu_id'        => 'primary-menu',
								'container' => '',
								'menu_class' => 'header-menu-list',
							) );
						?>
					</div>

					<?php
					if (false && function_exists('pll_the_languages')) :
						$langs = pll_the_languages(['raw'=>true]);
						?>
						<span class="lang">
							<?php foreach ($langs as $key => $lang) {
								?><a href="<?php echo $lang['url']; ?>" <?php if ($lang['current_lang']) echo "class=\"current_lang\""; ?>><?php echo strtoupper($lang['slug']); ?></a><?php
							} ?>
						</span>
					<?php endif; ?>

				</div>
			</div>
		</div>
	</header>

	<div class="content">