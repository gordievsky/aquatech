<?php

/**
 * Remove empty paragraphs created by wpautop()
 * @author Ryan Hamilton
 * @link   https://gist.github.com/Fantikerz/5557617
 */
function aqua_remove_empty_p( $content )
{
	$content = force_balance_tags( $content );
	$content = preg_replace( '#<p>\s*+(<br\s*/*>)?\s*</p>#i', '', $content );
	$content = preg_replace( '~\s?<p>(\s|&nbsp;)+</p>\s?~', '', $content );

	return $content;
}

//add_filter( 'the_content', 'aqua_remove_empty_p', 20, 1 );

function aqua_shortcode_empty_paragraph_fix( $content )
{

	$array = array(
		'<p>['    => '[',
		']</p>'   => ']',
		']<br />' => ']',
	);

	$content = strtr( $content, $array );

	return $content;
}

add_filter( 'the_content', 'aqua_shortcode_empty_paragraph_fix' );

/**
 * Adds custom classes to the array of body classes.
 *
 * @param array $classes Classes for the body element.
 *
 * @return array
 */
function aqua_body_classes( $classes )
{
	if ( is_front_page() )
	{
		$classes[] = 'front_page';
	}

	return $classes;
}

add_filter( 'body_class', 'aqua_body_classes' );

function aqua_nav_menu_link_attributes( $atts, $item, $args, $depth )
{
	if ($depth == 0) {
		$classes[] = 'header-menu__link';
	} else {
		$classes[] = 'header-menu-sublist__link';
	}

	foreach ($item->classes as $cls) {
		if (preg_match('/.*header\-menu__link_.*/', $cls)) {
			$classes[] = $cls;
			break;
		}
	}

	$atts['class'] = join(' ' , $classes);

	return $atts;
}

add_filter( 'nav_menu_link_attributes', 'aqua_nav_menu_link_attributes', 10, 4 );

function aqua_nav_menu_submenu_css_class( $classes, $args, $depth )
{
	$classes[] = 'header-menu-sublist';

	return $classes;
}

add_filter( 'nav_menu_submenu_css_class', 'aqua_nav_menu_submenu_css_class', 10, 3 );