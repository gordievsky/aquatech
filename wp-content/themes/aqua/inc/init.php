<?php

require_once 'actions/theme.php';
require_once 'post_types/post_types.php';
require_once 'post_types/actions.php';

AuqaPostTypes::instance()->init();
AuqaPostTypeActions::instance()->init();