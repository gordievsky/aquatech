<?php

class AuqaPostTypeActions
{
	private static $instance = null;

	public static function instance()
	{
		if ( is_null( self::$instance ) )
		{
			self::$instance = new self();
		}

		return self::$instance;
	}

	public function init()
	{
		add_action( 'admin_init', [ $this, 'register' ] );
	}

	public function register()
	{
		foreach ( AuqaPostTypes::get_list() as $post_type )
		{
			add_action( "manage_{$post_type}_posts_custom_column", [ $this, 'manage_posts_custom_column' ], 10 );
			add_filter( "manage_{$post_type}_posts_columns", [ $this, 'manage_posts_columns' ] );
		}
	}

	public function manage_posts_custom_column( $column )
	{
		if ( $column === 'featured_image' )
		{
			the_post_thumbnail( 'thumbnail' );
		}
		if ( $column === 'team_position' )
		{
			echo get_post_meta( get_the_ID(), 'aqua_team_position', true );
		}
	}

	public function manage_posts_columns( $columns )
	{
		$columns['team_position'] = __( 'Position', 'aqua' );
		$columns['featured_image'] = __( 'Featured Image', 'aqua' );

		return $columns;
	}

}