<?php

class AuqaPostTypes
{
	private static $instance = null;

	private static $list = [
		'aqua_team',
	];

	public static function instance()
	{
		if ( is_null( self::$instance ) )
		{
			self::$instance = new self();
		}

		return self::$instance;
	}

	public function init()
	{
		add_action( 'init', [ $this, 'register' ] );
	}

	public function register()
	{
		$this->team();
	}

	public static function get_list()
	{
		return self::$list;
	}

	private function team()
	{
		$args = [
			'public'   => true,
			'label'    => __( 'Our Team', 'aqua' ),
			'supports' => [ 'title', 'editor', 'thumbnail', 'custom-fields' ],
			'rewrite'  => [ 'slug' => 'team' ],
		];

		register_post_type( 'aqua_team', $args );
	}

}